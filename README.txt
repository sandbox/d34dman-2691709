Difference between l10n_client and 'core hack' logging.
  1.  l10n_client method doesn't kill any kittens (i.e. no hacking core).
  2.  core hack method logs multiple occurrance of string too.
  3.  core hack method doesn't need any extra modules.
  4.  core hack method works on default language english too.

How to set up Logging.
  Using l10n_client helper functions,
  1.  Download and install l10n_client module.
  2.  Add and enable another language to the site.
  3.  Make it default language (optional step).
  4.  Visit 'admin/config/regional/monitor-string-usage' and
      select 'l10n_client' as the usage method. Save.
  5.  Now browsing the site in languages other than english will start logging.

  Using core hack,
  1.  Apply the patch found in this modules directory to /includes/bootstrap.inc
  2.  Visit 'admin/config/regional/monitor-string-usage' and select 'core hack'
  3.  as the usage method. Save.
  4.  Browsing the site will start logging.

Access Logging Data
  1. Right now the logged data is in table named 'monitor_string_usage'
